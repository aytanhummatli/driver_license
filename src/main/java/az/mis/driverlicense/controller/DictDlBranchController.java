package az.mis.driverlicense.controller;

import az.mis.driverlicense.dao.dictDlBranch.DictDlBranchDao;
import az.mis.driverlicense.domain.DictDlBranch;
import az.mis.driverlicense.service.dictDlBranch.DictDlBranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictDlBranchController {


    @Autowired
    DictDlBranchService dictDlBranchService;


    @GetMapping("/listOfDlBranch")
    public ResponseEntity<List<DictDlBranch>> listOfDlBranch() {

        List<DictDlBranch> listOfDlBranch = new ArrayList<>();
        listOfDlBranch =dictDlBranchService.listOfDlBranch();

        return new ResponseEntity<List<DictDlBranch>>(listOfDlBranch, HttpStatus.OK);

    }



    


}
