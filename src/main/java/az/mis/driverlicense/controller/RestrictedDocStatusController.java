package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.Region;
import az.mis.driverlicense.domain.RestrictedDocStatus;
import az.mis.driverlicense.service.region.DictRegionService;
import az.mis.driverlicense.service.restrictedDocStatus.RestrictedDocStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class RestrictedDocStatusController {


    @Autowired
    RestrictedDocStatusService restrictedDocStatusService;


    @GetMapping("/listOfRestrictedDocStatus")
    public ResponseEntity<List<RestrictedDocStatus>> listOfRestrictedDocStatus() {

        List<RestrictedDocStatus> restrictedDocStatusList = new ArrayList<>();

        restrictedDocStatusList =restrictedDocStatusService.listOfRestrictedDocStatus();

        return new ResponseEntity<List<RestrictedDocStatus>>(restrictedDocStatusList, HttpStatus.OK);

    }





}
