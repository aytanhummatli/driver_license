package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.DictCourt;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.service.dictCourt.DictCourtService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictCourtController {


    @Autowired
    DictCourtService dictCourtService;


    @GetMapping("/listOfCourts")
    public ResponseEntity<List<DictCourt>> listOfCourts() {

        List<DictCourt> dictCourtList = new ArrayList<>();

        dictCourtList =dictCourtService.listOfCourts();

        return new ResponseEntity<List<DictCourt>>(dictCourtList, HttpStatus.OK);

    }


    @PostMapping("/insertCourt")
    public ResponseEntity<ExceptionModel> insertCourt(@RequestBody DictCourt court ) {

        return new ResponseEntity<ExceptionModel>(dictCourtService.insertCourt(court), HttpStatus.OK);

    }


    @PostMapping("/updateCourt")
    public ResponseEntity<ExceptionModel> updateCourt(@RequestBody DictCourt court) {

        return new ResponseEntity<ExceptionModel>(dictCourtService.updateCourt(court), HttpStatus.OK);

    }


    @PostMapping("/deleteCourt")
    public ResponseEntity<ExceptionModel> deleteCourt(@RequestBody DictCourt court) {

        ExceptionModel exceptionModel=new ExceptionModel();

        exceptionModel=dictCourtService.deleteCourt(court);

        System.out.println("deleteCourt "+exceptionModel.toString());

        return new ResponseEntity<ExceptionModel>(exceptionModel, HttpStatus.OK);

    }


}
