package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import az.mis.driverlicense.service.dictOffice.DictOfficeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictOfficeController {

    @Autowired
    DictOfficeService dictOfficeService;


    @GetMapping("/listOfOffices")
    public ResponseEntity<List<Office>> listOfOffices() {
        List<Office> listOfOffices = new ArrayList<>();
        listOfOffices =dictOfficeService.listOfOffices();
        return new ResponseEntity<List<Office>>(listOfOffices, HttpStatus.OK);
    }


    @GetMapping("/fullOfficeById/{userId}/{officeId}")
    public ResponseEntity<Office> fullOfficeById(@PathVariable int userId, @PathVariable int officeId) {

        Office office = new Office();
        office =dictOfficeService.listOfFullOfficesById(userId,officeId);
        return new ResponseEntity<Office>(office, HttpStatus.OK);
    }

    @GetMapping("/searchOfficeByName/{userId}/{name}")
    public ResponseEntity<List<Office>> listOfFullOfficesById(@PathVariable int userId, @PathVariable String name) {
        List<Office> listOfOffices = new ArrayList<>();
        listOfOffices =dictOfficeService.searchOfficeByName(userId,name);
        return new ResponseEntity<List<Office>>(listOfOffices, HttpStatus.OK);
    }

    @PostMapping("/insertOffice/{userId}")
    public ResponseEntity<ExceptionModel> insertOfficeType(@RequestBody Office office, @PathVariable int userId) {
        return new ResponseEntity<ExceptionModel>(dictOfficeService.insertOffice(office,userId), HttpStatus.OK);
    }

    @PostMapping("/updateOffice/{userId}")
    public ResponseEntity<ExceptionModel> updateOfficeType(@RequestBody Office office, @PathVariable int userId) {
        return new ResponseEntity<ExceptionModel>(dictOfficeService.updateOffice(office,userId), HttpStatus.OK);
    }

    @GetMapping("/deleteOffice/{userId}/{officeId}")
    public ResponseEntity<ExceptionModel> deleteOffice( @PathVariable int officeId, @PathVariable int userId) {
        return new ResponseEntity<ExceptionModel>(dictOfficeService.deleteOffice( userId,officeId), HttpStatus.OK);
    }




}
