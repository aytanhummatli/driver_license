package az.mis.driverlicense.controller;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.service.driverLicense.DriverLicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RequestMapping("/driverLicense")
@RestController

public class DriverLicenseController {

    @Autowired
    DriverLicenseService driverLicenseService;


    @GetMapping("/getAllFieldsByDLID/{dlId}")
     public ResponseEntity<DriverLicense> getAllFieldsByDLID(@PathVariable int dlId) {
        DriverLicense driverLicense = new DriverLicense();
        driverLicense = driverLicenseService.getAllFieldsByDLID(dlId);
        return new ResponseEntity<DriverLicense>(driverLicense, HttpStatus.OK);
       }


   @GetMapping("/listDlMaingridByOffice/{officeId}/{dlSstatus}")
    public ResponseEntity<List<DriverLicense>> listDlMaingridByOffice(@PathVariable int officeId,@PathVariable  String dlSstatus) {
      List<DriverLicense> driverLicenses = new ArrayList<>();
      return new ResponseEntity<List<DriverLicense>>(driverLicenseService.listDlMaingridByUser(officeId, dlSstatus), HttpStatus.OK);
      }


    @PostMapping("/listDlMaingridSearch/{searchLike}")
    public ResponseEntity<List<DriverLicense>> listDlMaingridSearch(@RequestBody DriverLicense driverLicense, @PathVariable  String searchLike) {
        System.out.println("driverLicense ="+driverLicense.toString());
        System.out.println("searchLike ="+searchLike);
        List<DriverLicense> driverLicenses = new ArrayList<>();
        return new ResponseEntity<List<DriverLicense>>(driverLicenseService.listDlMaingridSearch(driverLicense, searchLike ), HttpStatus.OK);
      }


    @PostMapping("/updateByDlId")
    public ResponseEntity<ExceptionModel> updateDlStatusByDlId(@RequestBody DriverLicense driverLicense) {
        System.out.println("driverLicense ="+driverLicense.toString());
               List<DriverLicense> driverLicenses = new ArrayList<>();
        return new ResponseEntity<ExceptionModel>(driverLicenseService.updateDlStatus(driverLicense), HttpStatus.OK);
    }


}
