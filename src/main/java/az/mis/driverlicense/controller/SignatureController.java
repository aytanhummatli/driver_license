package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Signature;
import az.mis.driverlicense.service.imageSignature.ImageSignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/signature")
@RestController

public class SignatureController {

    @Autowired
    ImageSignatureService imageSignatureService;



    @PostMapping("/insert/{userId}")
    public ResponseEntity<ExceptionModel> insertSignature(@RequestBody Signature signature, @PathVariable  int userId) {

        return new ResponseEntity<ExceptionModel>(imageSignatureService.insertSignature(userId,signature), HttpStatus.OK);
      }



}
