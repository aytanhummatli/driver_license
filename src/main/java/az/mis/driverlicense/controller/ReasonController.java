package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.Reason;
import az.mis.driverlicense.service.dictReason.DictReasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;



@RestController
@RequestMapping("/dict")
public class ReasonController {

    @Autowired
    DictReasonService dictReasonService;

    @GetMapping("/listOfReasons")
    public ResponseEntity<List<Reason>> listOfReasons() {
        List<Reason> reasons = new ArrayList<>();
        reasons =dictReasonService.listOfReasons();
        return new ResponseEntity<List<Reason>>(reasons, HttpStatus.OK);
    }



}
