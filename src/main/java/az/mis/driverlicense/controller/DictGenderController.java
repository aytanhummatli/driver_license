package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.DictGender;
import az.mis.driverlicense.domain.DictSchool;
import az.mis.driverlicense.service.dictGender.DictGenderService;
import az.mis.driverlicense.service.dictSchool.DictSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictGenderController {


    @Autowired
    DictGenderService dictGenderService;


    @GetMapping("/listOfGender")
    public ResponseEntity<List<DictGender>> listOfGender() {

        List<DictGender> listOfGender = new ArrayList<>();
        listOfGender =dictGenderService.listOfGender();

        return new ResponseEntity<List<DictGender>>(listOfGender, HttpStatus.OK);

    }





}
