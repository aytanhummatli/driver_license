package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.OfficeType;

import az.mis.driverlicense.service.officeType.OfficeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictOfficeTypeController {

    @Autowired
    OfficeTypeService officeTypeService;


    @GetMapping("/listOfficeTypes")
    public ResponseEntity<List<OfficeType>> listOfficeTypes() {

        List<OfficeType> officeTypeList = new ArrayList<>();

        officeTypeList =officeTypeService.listOfficeTypes();

        return new ResponseEntity<List<OfficeType>>(officeTypeList, HttpStatus.OK);

    }


    @PostMapping("/insertOfficeType/{userId}")
    public ResponseEntity<ExceptionModel> insertOfficeType(@RequestBody OfficeType officeType, @PathVariable int userId) {

        return new ResponseEntity<ExceptionModel>(officeTypeService.insertOfficeType(officeType,userId), HttpStatus.OK);

    }


    @PostMapping("/updateOfficeType/{userId}")
    public ResponseEntity<ExceptionModel> updateOfficeType(@RequestBody OfficeType officeType, @PathVariable int userId) {

        return new ResponseEntity<ExceptionModel>(officeTypeService.updateOfficeType(officeType,userId), HttpStatus.OK);

    }



}
