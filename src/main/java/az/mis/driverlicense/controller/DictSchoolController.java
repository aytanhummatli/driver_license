package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.DictSchool;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.service.dictSchool.DictSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictSchoolController {


    @Autowired
    DictSchoolService dictSchoolService;


    @GetMapping("/listOfSchools")
    public ResponseEntity<List<DictSchool>> listOfSchools() {

        List<DictSchool> listOfSchools = new ArrayList<>();
        listOfSchools =dictSchoolService.listOfSchools();
        System.out.println("listOfSchools"+listOfSchools);
        return new ResponseEntity<List<DictSchool>>(listOfSchools, HttpStatus.OK);

    }

    @GetMapping("/listOfSchoolsByName/{userId}/{name}")
    public ResponseEntity<List<DictSchool>> listOfSchoolsByName(@PathVariable int userId, @PathVariable String name) {

        List<DictSchool> listOfSchools = new ArrayList<>();
        listOfSchools =dictSchoolService.listOfSchoolsbyName(userId,name);

        return new ResponseEntity<List<DictSchool>>(listOfSchools, HttpStatus.OK);

    }

    @PostMapping("/insertSchool")
    public ResponseEntity<ExceptionModel> insertSchool(@RequestBody DictSchool school ) {

        return new ResponseEntity<ExceptionModel>(dictSchoolService.insertSchool(school), HttpStatus.OK);

    }


    @PostMapping("/updateSchool")
    public ResponseEntity<ExceptionModel> updateSchool(@RequestBody DictSchool school) {

        return new ResponseEntity<ExceptionModel>(dictSchoolService.updateSchool(school), HttpStatus.OK);

    }


    @PostMapping("/deleteSchool")
    public ResponseEntity<ExceptionModel> deleteSchool(@RequestBody DictSchool school) {

        return new ResponseEntity<ExceptionModel>(dictSchoolService.deleteSchool(school), HttpStatus.OK);

    }


}
