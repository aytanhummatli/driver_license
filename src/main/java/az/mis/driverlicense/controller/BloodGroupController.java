package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.DictCourt;
import az.mis.driverlicense.service.bloodGroup.DictBloodGroupService;
import az.mis.driverlicense.service.dictCourt.DictCourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class BloodGroupController {


    @Autowired
    DictBloodGroupService dictBloodGroupService;


    @GetMapping("/listOfBloodGroups")
    public ResponseEntity<List<BloodGroup>> listOfBloodGroups() {

        List<BloodGroup> bloodGroups = new ArrayList<>();

        bloodGroups =dictBloodGroupService.listOfBloodGroup();

        return new ResponseEntity<List<BloodGroup>>(bloodGroups, HttpStatus.OK);

    }





}
