package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.Status;

import az.mis.driverlicense.service.status.DictStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class StatusController {


    @Autowired
    DictStatusService statusService;


    @GetMapping("/listOfStatus")
    public ResponseEntity<List<Status>> listOfStatus() {

        List<Status> statusList = new ArrayList<>();

        statusList =statusService.listOfStatus();

        return new ResponseEntity<List<Status>>(statusList, HttpStatus.OK);

    }





}
