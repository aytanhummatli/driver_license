package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.PersonMobile;
import az.mis.driverlicense.service.bloodGroup.DictBloodGroupService;
import az.mis.driverlicense.service.personMobile.PersonMobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/personMobile")
public class PersonMobileController {


    @Autowired
    PersonMobileService personMobileService;


    @GetMapping("/list/{personId}")
    public ResponseEntity<List<PersonMobile>> listOfPersonMobiles(@PathVariable int personId) {

        List<PersonMobile> personMobiles = new ArrayList<>();

        personMobiles = personMobileService.listPersonMobile(personId);

        return new ResponseEntity<List<PersonMobile>>(personMobiles, HttpStatus.OK);

    }


}
