package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.Region;
import az.mis.driverlicense.service.bloodGroup.DictBloodGroupService;
import az.mis.driverlicense.service.region.DictRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class RegionController {


    @Autowired
    DictRegionService regionService;


    @GetMapping("/listOfRegion")
    public ResponseEntity<List<Region>> listOfRegion() {

        List<Region> regionList = new ArrayList<>();

        regionList =regionService.listOfRegion();

        return new ResponseEntity<List<Region>>(regionList, HttpStatus.OK);

    }





}
