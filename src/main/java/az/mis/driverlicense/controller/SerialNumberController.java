package az.mis.driverlicense.controller;

import az.mis.driverlicense.domain.DlSerialNumber;
import az.mis.driverlicense.service.serialNumber.SerialNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/serialNumber")
public class SerialNumberController {

    @Autowired
    SerialNumberService serialNumberService;

    @GetMapping("/getNew/{officeId}")
    public ResponseEntity<DlSerialNumber> newSerialNumber(@PathVariable int officeId) {

        DlSerialNumber dlSerialNumber = new DlSerialNumber();

        dlSerialNumber = serialNumberService.getNewSerialNumber(officeId);

        return new ResponseEntity<DlSerialNumber>(dlSerialNumber, HttpStatus.OK);

    }

    @GetMapping("/getListByOfficeId/{userId}/{officeId}")
    public ResponseEntity<List<DlSerialNumber>> getListByOfficeId(  @PathVariable int userId, @PathVariable int officeId) {

        List<DlSerialNumber> dlSerialNumbers = new ArrayList<>();

        dlSerialNumbers = serialNumberService.getSerialNumberbyOffice(officeId, userId);

        return new ResponseEntity<List<DlSerialNumber>>(dlSerialNumbers, HttpStatus.OK);

    }


}


