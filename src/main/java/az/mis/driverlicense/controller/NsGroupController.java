package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.NsGroup;
import az.mis.driverlicense.service.nsGroup.NsGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequestMapping("/nsGroup")
@RestController
public class NsGroupController {

    @Autowired
    NsGroupService nsGroupService;


    @PostMapping("/insert")
    public ResponseEntity<ExceptionModel> insertNsGroup(@RequestBody NsGroup nsGroup) {

        return new ResponseEntity<ExceptionModel>(nsGroupService.insertNsGroup(nsGroup), HttpStatus.OK);
    }


    @GetMapping("/getListByOfficeId/{userId}/{officeId}")
    public ResponseEntity<List<NsGroup>> getListByOfficeId(@PathVariable int userId, @PathVariable int officeId) {

        return new ResponseEntity<List<NsGroup>> (nsGroupService.getNsGroupsbyOffice(userId,officeId), HttpStatus.OK);
    }


}