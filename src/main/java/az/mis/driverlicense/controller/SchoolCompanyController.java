package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.SchoolCompany;

import az.mis.driverlicense.service.schoolCompany.DictSchoolCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dict")
public class SchoolCompanyController {


    @Autowired
    DictSchoolCompanyService dictSchoolCompanyService;


    @GetMapping("/listOfSchoolCompany")
    public ResponseEntity<List<SchoolCompany>> listOfSchoolCompany() {

        List<SchoolCompany> listOfSchoolCompany = new ArrayList<>();

        listOfSchoolCompany =dictSchoolCompanyService.listOfSchoolCompany();

        return new ResponseEntity<List<SchoolCompany>>(listOfSchoolCompany, HttpStatus.OK);

    }


    @PostMapping("/insertSchoolCompany")
    public ResponseEntity<ExceptionModel> insertSchool(@RequestBody SchoolCompany school ) {

        return new ResponseEntity<ExceptionModel>(dictSchoolCompanyService.insertSchoolCompany(school), HttpStatus.OK);

    }


    @PostMapping("/updateSchoolCompany")
    public ResponseEntity<ExceptionModel> updateSchool(@RequestBody SchoolCompany school) {

        return new ResponseEntity<ExceptionModel>(dictSchoolCompanyService.updateSchoolCompany(school), HttpStatus.OK);

    }


    @PostMapping("/deleteSchoolCompany")
    public ResponseEntity<ExceptionModel> deleteSchool(@RequestBody SchoolCompany school) {

        ExceptionModel exceptionModel=new ExceptionModel();
        exceptionModel=dictSchoolCompanyService.deleteSchoolCompany(school);

        System.out.println("deleteSchoolCompany "+exceptionModel.toString());

        return new ResponseEntity<ExceptionModel>(exceptionModel, HttpStatus.OK);

    }


}
