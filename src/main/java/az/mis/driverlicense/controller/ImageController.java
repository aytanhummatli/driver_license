package az.mis.driverlicense.controller;


import az.mis.driverlicense.domain.*;
import az.mis.driverlicense.service.imageSignature.ImageSignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/image")
@RestController

public class ImageController {

    @Autowired
    ImageSignatureService imageSignatureService;


    @PostMapping("/insert/{userId}")
    public ResponseEntity<ExceptionModel> insertImage(@RequestBody Image image, @PathVariable  int userId) {

        return new ResponseEntity<ExceptionModel>(imageSignatureService.insertImage(userId,image), HttpStatus.OK);
      }


    @GetMapping("/getImgSign/{dlId}")
    public ResponseEntity<DriverLicense> getImageSignatureByDlId(@PathVariable int dlId) {
        DriverLicense driverLicense = new DriverLicense();
        driverLicense = imageSignatureService.getImageSignatureByDlId(dlId);
        System.out.println("getImageSignatureByDlId  "+driverLicense.toString());
        return new ResponseEntity<DriverLicense>(driverLicense, HttpStatus.OK);
    }


    @GetMapping("/getBkgrImg/{officeId}")
    public ResponseEntity<Image> getBkgrImg(@PathVariable int officeId) {
        Image image=new Image();
        image.setImageData(imageSignatureService.getBackgroundImageByOfficeId(officeId));
        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }

    @GetMapping("/getStampImg/{officeId}")
    public ResponseEntity<Image> getStampImg(@PathVariable int officeId) {

        Image image=new Image();
        image.setImageData(imageSignatureService.getStampImageByOfficeId(officeId));
        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }



}
