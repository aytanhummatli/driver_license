package az.mis.driverlicense.dao.restrictedDocStatus;



import az.mis.driverlicense.domain.RestrictedDocStatus;

import java.util.List;

public interface RestrictedDocStatusDao {

    public List<RestrictedDocStatus> listOfRestrictedDocStatus ();
}
