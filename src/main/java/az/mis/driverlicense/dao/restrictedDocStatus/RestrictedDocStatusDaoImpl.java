package az.mis.driverlicense.dao.restrictedDocStatus;



import az.mis.driverlicense.domain.Region;
import az.mis.driverlicense.domain.RestrictedDocStatus;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class RestrictedDocStatusDaoImpl implements RestrictedDocStatusDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<RestrictedDocStatus> listOfRestrictedDocStatus () {

        Locale.setDefault(Locale.ENGLISH);

        List<RestrictedDocStatus> restrictedDocStatusList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_restricted_doc_status");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("restricted_doc_status_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(RestrictedDocStatus.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            restrictedDocStatusList = (List) map.get("restricted_doc_status_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return restrictedDocStatusList;
    }





}
