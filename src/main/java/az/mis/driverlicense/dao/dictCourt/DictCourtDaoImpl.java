package az.mis.driverlicense.dao.dictCourt;


import az.mis.driverlicense.domain.DictCourt;

import az.mis.driverlicense.domain.ExceptionModel;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictCourtDaoImpl implements DictCourtDao {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public ExceptionModel deleteCourt(DictCourt dictCourt) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("delete_court");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));


            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",dictCourt.getId());
            parameterSource.addValue("p_user_id",dictCourt.getInsertUserId());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

    public ExceptionModel updateCourt(DictCourt dictCourt) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("update_court");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",dictCourt.getId());
            parameterSource.addValue("p_user_id",dictCourt.getInsertUserId());
            parameterSource.addValue("p_name",dictCourt.getName());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("updateCourt "+exception);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }



    public ExceptionModel insertCourt(DictCourt dictCourt) {

        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("insert_court");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_user_id",dictCourt.getInsertUserId());
            parameterSource.addValue("p_name",dictCourt.getName());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            exception.setHelperInt((Integer) map.get("p_returning_id"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }



    public List<DictCourt> listOfCourts() {

        Locale.setDefault(Locale.ENGLISH);

        List<DictCourt> dictCourts = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_courts");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("court_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(DictCourt.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            dictCourts = (List) map.get("court_list");


        } catch (Exception e) {

            e.printStackTrace();
        }

        return dictCourts;
    }





}
