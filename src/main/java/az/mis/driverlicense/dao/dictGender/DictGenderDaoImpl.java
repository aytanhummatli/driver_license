package az.mis.driverlicense.dao.dictGender;


import az.mis.driverlicense.domain.DictGender;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictGenderDaoImpl implements DictGenderDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<DictGender> listOfGender() {

        Locale.setDefault(Locale.ENGLISH);

        List<DictGender> dictGenders = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_genders");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("gender_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(DictGender.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            dictGenders = (List) map.get("gender_list");


        } catch (Exception e) {

            e.printStackTrace();
        }

        return dictGenders;
    }





}
