package az.mis.driverlicense.dao.dictGender;

import az.mis.driverlicense.domain.DictGender;

import java.util.List;

public interface DictGenderDao {
    public List<DictGender> listOfGender();
}
