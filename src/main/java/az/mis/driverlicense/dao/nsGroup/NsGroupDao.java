package az.mis.driverlicense.dao.nsGroup;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.NsGroup;

import java.util.List;


public interface NsGroupDao {
    public ExceptionModel insertNsGroup(NsGroup nsGroup);
    public List<NsGroup> getNsGroupsbyOffice(int  userId , int officeId);
}
