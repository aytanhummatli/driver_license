package az.mis.driverlicense.dao.nsGroup;

import az.mis.driverlicense.dao.driverLicense.DriverLicenseDao;
import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.NsGroup;
import az.mis.driverlicense.mapper.DriverLicenseMapper;
import az.mis.driverlicense.mapper.DriverLicenseMapper2;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class NsGroupDaoImpl implements NsGroupDao {


    @Autowired
    JdbcTemplate jdbcTemplate;
//
//    public List<DriverLicense> listDlMaingridByUser(int officeId, String dlSstatus) {
//
//        Locale.setDefault(Locale.ENGLISH);
//
//        List<DriverLicense> driverLicenses = new ArrayList<>();
//
//        try {
//
//            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
//                    .withCatalogName("LicensePKG").withProcedureName("list_dl_maingrid_by_office");
//
//            simpleJdbcCall.setAccessCallParameterMetaData(false);
//
//            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_office_id", OracleTypes.INTEGER));
//
//            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_status", OracleTypes.CHAR));
//
//            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new DriverLicenseMapper()));
//
//            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
//
//            parameterSource.addValue("p_office_id", officeId);
//
//            parameterSource.addValue("p_dl_status", dlSstatus);
//
//            Map map = simpleJdbcCall.execute(parameterSource);
//
//            driverLicenses = (List) map.get("cur");
//
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        return driverLicenses;
//    }


    public  List<NsGroup> getNsGroupsbyOffice(int  userId ,int officeId) {

        Locale.setDefault(Locale.ENGLISH);

        List<NsGroup> nsGroupArrayList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("get_ns_groups_byoffice");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_office_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,new BeanPropertyRowMapper(NsGroup.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_office_id",officeId);
            parameterSource.addValue("p_user_id",userId);

            Map map = simpleJdbcCall.execute(parameterSource);

            nsGroupArrayList = (List) map.get("cur");

            System.out.println("serialNumberArrayList  " + nsGroupArrayList.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return nsGroupArrayList;
    }

    public ExceptionModel insertNsGroup(NsGroup nsGroup) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("insert_ns_group");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_GROUP_ID", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_DL_SERIA", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_FROM_NUMBER", OracleTypes.CHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_TO_NUMBER", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_INSERT_USER_ID", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_IS_ACTIVE", OracleTypes.CHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_DL_OFFICE_ID", OracleTypes.CHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("returning_id", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_GROUP_ID", nsGroup.getGroup().getId());
            parameterSource.addValue("p_DL_SERIA", nsGroup.getSeria());
            parameterSource.addValue("p_FROM_NUMBER",nsGroup.getFromNumber());
            parameterSource.addValue("p_TO_NUMBER", nsGroup.getToNumber());
            parameterSource.addValue("p_INSERT_USER_ID", nsGroup.getInsertUserId());
            parameterSource.addValue("p_IS_ACTIVE", nsGroup.getIsActive());
            parameterSource.addValue("p_DL_OFFICE_ID", nsGroup.getOffice().getId());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));

            exception.setExceptionMessage((String) map.get("exception_message"));

            exception.setHelperInt((Integer) map.get("returning_id"));


        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }
}
