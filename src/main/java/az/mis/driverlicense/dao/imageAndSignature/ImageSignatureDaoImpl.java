package az.mis.driverlicense.dao.imageAndSignature;

import az.mis.driverlicense.dao.driverLicense.DriverLicenseDao;
import az.mis.driverlicense.domain.DriverLicense;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Image;
import az.mis.driverlicense.domain.Signature;
import az.mis.driverlicense.mapper.DriverLicenseMapper;
import az.mis.driverlicense.mapper.DriverLicenseMapper2;
import az.mis.driverlicense.mapper.ImgSignMapper;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class ImageSignatureDaoImpl implements ImageSignatureDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public String getStampImageByOfficeId(int officeId) {

        Locale.setDefault(Locale.ENGLISH);

        String stampImage = "";
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("img_signature").withProcedureName("get_stamp_image_byofficeid");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("stamp_image", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id", officeId);

            Map map = simpleJdbcCall.execute(parameterSource);

            stampImage = (String) map.get("stamp_image");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return stampImage;
    }


    public String getBackgroundImageByOfficeId(int officeId) {

        Locale.setDefault(Locale.ENGLISH);

        String backGroundImage = "";
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("img_signature").withProcedureName("get_backgr_image_byofficeid");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("background_image", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id", officeId);

            Map map = simpleJdbcCall.execute(parameterSource);

            backGroundImage = (String) map.get("background_image");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return backGroundImage;
    }

    public DriverLicense getImageSignatureByDlId(int dlId) {

        Locale.setDefault(Locale.ENGLISH);

        DriverLicense driverLicense = new DriverLicense();

        List<DriverLicense> driverLicenses = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("img_signature").withProcedureName("get_image_signature_bydlId");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new ImgSignMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_dl_id", dlId);

            Map map = simpleJdbcCall.execute(parameterSource);


            driverLicenses = (List<DriverLicense>) map.get("cur");

            for (DriverLicense elem : driverLicenses) {
                driverLicense = elem;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return driverLicense;
    }


    public ExceptionModel insertSignature(int userId, Signature signature) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception = new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("img_signature").withProcedureName("insert_signature");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_signature", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_DL_id", signature.getDlId());
            parameterSource.addValue("user_id", userId);
            parameterSource.addValue("p_signature", signature.getImageData());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));
            exception.setExceptionMessage((String) map.get("exception_message"));


        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }


    public ExceptionModel insertImage(int userId, Image image) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception = new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("img_signature").withProcedureName("insert_person_image");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_image", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_DL_id", image.getDlId());
            parameterSource.addValue("user_id", userId);
            parameterSource.addValue("p_image", image.getImageData());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));

            exception.setExceptionMessage((String) map.get("exception_message"));

            System.out.println("exceptionnn  " + exception.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

}
