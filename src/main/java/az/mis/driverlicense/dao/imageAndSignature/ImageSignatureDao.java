package az.mis.driverlicense.dao.imageAndSignature;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Image;
import az.mis.driverlicense.domain.Signature;


public interface ImageSignatureDao {
    public ExceptionModel insertSignature(int userId, Signature signature) ;
    public ExceptionModel insertImage(int userId, Image image);
    public DriverLicense getImageSignatureByDlId(int dlId);


    public String getStampImageByOfficeId(int officeId);
    public String getBackgroundImageByOfficeId(int officeId);

}
