package az.mis.driverlicense.dao.dictDlBranch;


import az.mis.driverlicense.domain.DictDlBranch;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictDlBranchDaoImpl implements DictDlBranchDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<DictDlBranch> listOfDlBranch() {

        Locale.setDefault(Locale.ENGLISH);

        List<DictDlBranch> dictDlBranches = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_dlBranch");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("branch_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(DictDlBranch.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            dictDlBranches = (List) map.get("branch_list");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return dictDlBranches;
    }





}
