package az.mis.driverlicense.dao.dictDlBranch;

import az.mis.driverlicense.domain.DictDlBranch;

import java.util.List;

public interface DictDlBranchDao {
    public List<DictDlBranch> listOfDlBranch();
}
