package az.mis.driverlicense.dao.reason;


import az.mis.driverlicense.domain.Reason;

import java.util.List;

public interface DictReasonDao {

    public List<Reason> listOfReasons();
}
