package az.mis.driverlicense.dao.reason;


import az.mis.driverlicense.domain.Reason;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictReasonDaoImpl implements DictReasonDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Reason> listOfReasons() {

        Locale.setDefault(Locale.ENGLISH);

        List<Reason> reasons = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_reason");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("reason_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(Reason.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            reasons = (List) map.get("reason_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return reasons;
    }


}
