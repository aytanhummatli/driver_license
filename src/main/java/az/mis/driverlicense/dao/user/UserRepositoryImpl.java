package az.mis.driverlicense.dao.user;

import az.mis.driverlicense.domain.Rule;
import az.mis.driverlicense.domain.Users;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Optional<Users> findByName(String username) {

        Locale.setDefault(Locale.ENGLISH);

        Users myUser = new Users();

        List<Users> userList = new ArrayList<>();

        List<Rule> ruleList = new ArrayList<Rule>();


        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")

                    .withCatalogName("security").withProcedureName("getUserByName");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("username", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("userInfo", OracleTypes.CURSOR, new BeanPropertyRowMapper(Users.class)));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("ruleList", OracleTypes.CURSOR, new BeanPropertyRowMapper(Rule.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("username", username);

            Map map = simpleJdbcCall.execute(parameterSource);

            userList = (List) map.get("userInfo");

            for (Users elem : userList) {

                myUser = elem;
            }

            ruleList = (List<Rule>) map.get("ruleList");

            System.out.println("rulelist" + map.get("ruleList"));

            myUser.setRules(ruleList);


        } catch (Exception e) {

            e.printStackTrace();
        }

        System.out.println("user details from dao :" + myUser.toString());

        return Optional.ofNullable(myUser);

    }

}