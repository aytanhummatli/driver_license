package az.mis.driverlicense.dao.user;


import az.mis.driverlicense.domain.Users;

import java.util.Optional;

public interface UserRepository {
   public Optional<Users> findByName(String username);
}
