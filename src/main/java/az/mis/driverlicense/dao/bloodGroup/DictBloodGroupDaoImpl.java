package az.mis.driverlicense.dao.bloodGroup;


import az.mis.driverlicense.domain.BloodGroup;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictBloodGroupDaoImpl implements DictBloodGroupDao {


    @Autowired
    JdbcTemplate jdbcTemplate;



    public List<BloodGroup> listOfBloodGroup() {

        Locale.setDefault(Locale.ENGLISH);

        List<BloodGroup> bloodGroups = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_blood");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("blood_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(BloodGroup.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            bloodGroups = (List) map.get("blood_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return bloodGroups;
    }





}
