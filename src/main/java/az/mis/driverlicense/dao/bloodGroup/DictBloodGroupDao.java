package az.mis.driverlicense.dao.bloodGroup;

import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.DictCourt;

import java.util.List;

public interface DictBloodGroupDao {

    public List<BloodGroup> listOfBloodGroup();
}
