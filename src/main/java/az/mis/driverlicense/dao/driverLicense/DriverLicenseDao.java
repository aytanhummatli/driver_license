package az.mis.driverlicense.dao.driverLicense;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import org.springframework.stereotype.Repository;

import java.util.List;



public interface DriverLicenseDao {
    public List<DriverLicense> listDlMaingridByUser(int officeId, String dlSstatus);

    public DriverLicense getAllFieldsByDLID(int dlId);
    public List<DriverLicense> listDlMaingridSearch(DriverLicense driverLicense,String searchLike);

    public ExceptionModel updateDlStatus(DriverLicense driverLicense);
}
