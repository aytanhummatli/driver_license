package az.mis.driverlicense.dao.driverLicense;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.mapper.DriverLicenseMapper;
import az.mis.driverlicense.mapper.DriverLicenseMapper2;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.util.*;


@Repository
public class DriverLicenseDaoImpl implements DriverLicenseDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<DriverLicense> listDlMaingridByUser(int officeId, String dlSstatus) {

        Locale.setDefault(Locale.ENGLISH);

        List<DriverLicense> driverLicenses = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("list_dl_maingrid_by_office");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_office_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_status", OracleTypes.CHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new DriverLicenseMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_office_id", officeId);

            parameterSource.addValue("p_dl_status", dlSstatus);

            Map map = simpleJdbcCall.execute(parameterSource);

            driverLicenses = (List) map.get("cur");


        } catch (Exception e) {

            e.printStackTrace();
        }

        return driverLicenses;
    }


    public List<DriverLicense> listDlMaingridSearch(DriverLicense driverLicense,String searchLike) {

        Locale.setDefault(Locale.ENGLISH);

        List<DriverLicense> driverLicenses = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("list_dl_maingrid_search");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_series", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dl_number", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_v_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_v_surname", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_v_fname", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_v_birth_date", OracleTypes.DATE));

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_v_search_like", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR, new DriverLicenseMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_dl_series", driverLicense.getDlSerialNumber().getSeria());

            parameterSource.addValue("p_dl_number", driverLicense.getDlSerialNumber().getDlNumber());

            parameterSource.addValue("p_v_name", driverLicense.getPerson().getName());

            parameterSource.addValue("p_v_surname", driverLicense.getPerson().getSurName());

            parameterSource.addValue("p_v_fname", driverLicense.getPerson().getFatherName());

            parameterSource.addValue("p_v_birth_date", driverLicense.getPerson().getBirthday());

            parameterSource.addValue("p_v_search_like", searchLike);

            System.out.println(parameterSource.getValues());

            Map map = simpleJdbcCall.execute(parameterSource);

            driverLicenses = (List) map.get("cur");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return driverLicenses;
    }


    public DriverLicense getAllFieldsByDLID(int dlId) {

        Locale.setDefault(Locale.ENGLISH);

        List<DriverLicense> driverLicenses = new ArrayList<>();

        DriverLicense dlObject=new DriverLicense();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("get_all_fields_by_DLID");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_DL_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("dlObject", OracleTypes.CURSOR, new DriverLicenseMapper2()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_DL_id", dlId);

            Map map = simpleJdbcCall.execute(parameterSource);

            driverLicenses = (List) map.get("dlObject");


            for (DriverLicense  elem :driverLicenses){

                dlObject=elem;
            }


        } catch (Exception e) {

            e.printStackTrace();
        }

        return dlObject;
    }


    public ExceptionModel updateDlStatus(DriverLicense driverLicense) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("update_status_by_dlid");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_dlid", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_status", OracleTypes.CHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_dlid", driverLicense.getId());
            parameterSource.addValue("p_user_id", driverLicense.getUserId());
            parameterSource.addValue("p_status", driverLicense.getStatus());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));

            exception.setExceptionMessage((String) map.get("exception_message"));

            System.out.println("exceptionnn  from  updateDlStatus  "+exception.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }
}
