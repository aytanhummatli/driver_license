package az.mis.driverlicense.dao.dictOffice;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import az.mis.driverlicense.mapper.OfficeMapper;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictOfficeDaoImpl implements DictOfficeDao {


    @Autowired
    JdbcTemplate jdbcTemplate;


    public ExceptionModel deleteOffice(int userId, int  officeId) {

        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("delete_office");
            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",officeId);
            parameterSource.addValue("p_user_id",userId);

            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("exception  "+exception.toString());
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }


    public ExceptionModel updateOffice(Office office, int userId) {

        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("update_office");
            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name_en", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_orderBy", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_officeType_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_stampimage", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_backimage", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",office.getId());
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_name",office.getName());
            parameterSource.addValue("p_name_en",office.getNameEn());
            parameterSource.addValue("p_orderBy",office.getOrderBy());
            parameterSource.addValue("p_officeType_id",office.getOfficeType().getId());
            parameterSource.addValue("p_stampimage",office.getStampImageDate());
            parameterSource.addValue("p_backimage", office.getBackgroundImageDate());

            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("exception"+exception.toString());
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }


    public ExceptionModel insertOffice(Office office, int userId) {

        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("insert_office");
            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name_en", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_orderBy", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_officeType_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_stampimage", OracleTypes.CLOB));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_backimage", OracleTypes.CLOB));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_name",office.getName());
            parameterSource.addValue("p_name_en",office.getNameEn());
            parameterSource.addValue("p_orderBy",office.getOrderBy());
            parameterSource.addValue("p_officeType_id",office.getOfficeType().getId());
//           parameterSource.addValue("p_stampimage","");
//            parameterSource.addValue("p_backimage", "");
            parameterSource.addValue("p_stampimage",office.getStampImageDate());
            parameterSource.addValue("p_backimage", office.getBackgroundImageDate());
            System.out.println("office"+office.toString());
            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            exception.setHelperInt((Integer) map.get("p_returning_id"));
            System.out.println("office insert exception"+exception);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }




    public  Office  listOfFullOfficesById(int userId,int officeId) {

        Locale.setDefault(Locale.ENGLISH);

        List<Office> full_offices = new ArrayList<>();
        Office office=new Office();
        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_full_offices_byId");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("full_office_list", OracleTypes.CURSOR, new OfficeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_id",officeId);
            Map map = simpleJdbcCall.execute(parameterSource);

            full_offices = (List) map.get("full_office_list");

            System.out.println(full_offices.toString());


            for (Office elem:full_offices) {

                office=elem;

            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return office;
    }



    public List<Office> listOfOffices() {

        Locale.setDefault(Locale.ENGLISH);

        List<Office> offices = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_offices");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("office_list", OracleTypes.CURSOR, new OfficeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            offices = (List) map.get("office_list");
            System.out.println(offices.toString());
        } catch (Exception e) {

            e.printStackTrace();
        }

        return offices;
    }

    public   List<Office> searchOfficeByName( int userId, String name) {

        Locale.setDefault(Locale.ENGLISH);

        List<Office> offices = new ArrayList<>();



        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_offices");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("office_list", OracleTypes.CURSOR, new OfficeMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_name",name);
            Map map = simpleJdbcCall.execute(parameterSource);

            offices = (List) map.get("office_list");

            System.out.println(offices.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return offices;
    }




}
