package az.mis.driverlicense.dao.personMobile;

import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.PersonMobile;

import java.util.List;

public interface PersonMobileDao {
    public List<PersonMobile> listPersonMobile(int personId);
}
