package az.mis.driverlicense.dao.personMobile;


import az.mis.driverlicense.domain.PersonMobile;

import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class PersonMobileDaoImpl implements PersonMobileDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<PersonMobile> listPersonMobile(int personId) {

        Locale.setDefault(Locale.ENGLISH);

        List<PersonMobile> personMobileList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("person").withProcedureName("get_person_numbers");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_person_id", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("numberList", OracleTypes.CURSOR, new BeanPropertyRowMapper(PersonMobile.class) ));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_person_id", personId);

            Map map = simpleJdbcCall.execute(parameterSource);

            personMobileList = (List) map.get("numberList");


        } catch (Exception e) {

            e.printStackTrace();
        }

        return personMobileList;
    }



}
