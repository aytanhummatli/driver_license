package az.mis.driverlicense.dao.dictSchoolDao;

import az.mis.driverlicense.domain.DictSchool;
import az.mis.driverlicense.domain.ExceptionModel;

import java.util.List;

public interface DictSchoolDao {
    public List<DictSchool> listOfSchools();
    public  List<DictSchool>  listOfSchoolsbyName( int userId, String name);
    public ExceptionModel updateSchool(DictSchool dictSchool);
    public ExceptionModel insertSchool(DictSchool dictSchool);
    public ExceptionModel deleteSchool(DictSchool dictSchool);
}
