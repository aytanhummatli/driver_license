package az.mis.driverlicense.dao.dictSchoolDao;


import az.mis.driverlicense.domain.DictSchool;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import az.mis.driverlicense.mapper.DictSchoolMapper;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictSchoolDaoImpl implements DictSchoolDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public ExceptionModel deleteSchool(DictSchool dictSchool) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("delete_school");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));


            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",dictSchool.getId());
            parameterSource.addValue("p_user_id",dictSchool.getInsertUserId());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

    public ExceptionModel updateSchool(DictSchool dictSchool) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("update_school");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_address", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_mobileNumber", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_phoneNumber", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_voen", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_person", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_SCHOOL_COMPANY_ID", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",dictSchool.getId());
            parameterSource.addValue("p_user_id",dictSchool.getInsertUserId());
            parameterSource.addValue("p_name",dictSchool.getName());
            parameterSource.addValue("p_address",dictSchool.getAddress());
            parameterSource.addValue("p_mobileNumber",dictSchool.getMobileNumber());
            parameterSource.addValue("p_phoneNumber",dictSchool.getPhoneNumber());
            parameterSource.addValue("p_voen",dictSchool.getVoen());
            parameterSource.addValue("p_person", dictSchool.getPerson());
            parameterSource.addValue("p_SCHOOL_COMPANY_ID", dictSchool.getSchoolCompany().getId());

            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("updateSchool "+exception);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }



    public ExceptionModel insertSchool(DictSchool dictSchool) {

        System.out.println("dddd"+dictSchool.toString());
        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("insert_school");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_address", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_mobileNumber", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_phoneNumber", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_voen", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_person", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_SCHOOL_COMPANY_ID", OracleTypes.INTEGER));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_user_id",dictSchool.getInsertUserId());
            parameterSource.addValue("p_name",dictSchool.getName());
            parameterSource.addValue("p_address",dictSchool.getAddress());
            parameterSource.addValue("p_mobileNumber",dictSchool.getMobileNumber());
            parameterSource.addValue("p_phoneNumber",dictSchool.getPhoneNumber());
            parameterSource.addValue("p_voen",dictSchool.getVoen());
            parameterSource.addValue("p_person", dictSchool.getPerson());
            parameterSource.addValue("p_SCHOOL_COMPANY_ID", dictSchool.getSchoolCompany().getId());

            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            exception.setHelperInt((Integer) map.get("p_returning_id"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }


    public List<DictSchool> listOfSchools() {

        Locale.setDefault(Locale.ENGLISH);

        List<DictSchool> dictSchools = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_schools");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("school_list", OracleTypes.CURSOR,   new DictSchoolMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            dictSchools = (List) map.get("school_list");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return dictSchools;
    }


    public  List<DictSchool>  listOfSchoolsbyName( int userId, String name) {

        Locale.setDefault(Locale.ENGLISH);

        List<DictSchool> dictSchools = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("school_list_byname");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("school_list", OracleTypes.CURSOR, new DictSchoolMapper()));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_name",name);
            Map map = simpleJdbcCall.execute(parameterSource);

            dictSchools = (List) map.get("school_list");


        } catch (Exception e) {

            e.printStackTrace();
        }

        return dictSchools;
    }


}
