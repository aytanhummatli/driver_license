package az.mis.driverlicense.dao.schoolCompany;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.SchoolCompany;

import java.util.List;

public interface DictSchoolCompanyDao {
    public ExceptionModel deleteSchoolCompany(SchoolCompany schoolCompany);
    public ExceptionModel updateSchoolCompany(SchoolCompany schoolCompany);
    public ExceptionModel insertSchoolCompany(SchoolCompany schoolCompany);
    public List<SchoolCompany> listOfSchoolCompany();
}
