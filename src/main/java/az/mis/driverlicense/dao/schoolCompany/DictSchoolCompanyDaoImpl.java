package az.mis.driverlicense.dao.schoolCompany;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.SchoolCompany;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictSchoolCompanyDaoImpl implements DictSchoolCompanyDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public ExceptionModel deleteSchoolCompany(SchoolCompany schoolCompany) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("delete_school_company");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));


            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",schoolCompany.getId());
            parameterSource.addValue("p_user_id",schoolCompany.getInsertUserId());

            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("deleteSchoolCompany "+exception.toString());
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

    public ExceptionModel updateSchoolCompany(SchoolCompany schoolCompany) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("update_school_company");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_id",schoolCompany.getId());
            parameterSource.addValue("p_user_id",schoolCompany.getInsertUserId());
            parameterSource.addValue("p_name",schoolCompany.getName());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            System.out.println("updateschoolCompany "+exception);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }



    public ExceptionModel insertSchoolCompany(SchoolCompany schoolCompany) {

        Locale.setDefault(Locale.ENGLISH);
        ExceptionModel exception=new ExceptionModel();
        try {
            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("insert_school_company");
            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_user_id",schoolCompany.getInsertUserId());
            parameterSource.addValue("p_name",schoolCompany.getName());


            Map map = simpleJdbcCall.execute(parameterSource);
            exception.setExceptionCode(String.valueOf(map.get("exception_code")));
            exception.setExceptionMessage((String) map.get("exception_message"));
            exception.setHelperInt((Integer) map.get("p_returning_id"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

    public List<SchoolCompany> listOfSchoolCompany() {

        Locale.setDefault(Locale.ENGLISH);

        List<SchoolCompany> schoolCompanies = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_school_company");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("school_company_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(SchoolCompany.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            schoolCompanies = (List) map.get("school_company_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return schoolCompanies;
    }





}
