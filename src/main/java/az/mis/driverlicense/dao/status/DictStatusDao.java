package az.mis.driverlicense.dao.status;


import az.mis.driverlicense.domain.Status;

import java.util.List;

public interface DictStatusDao {

    public List<Status> listOfStatus();
}
