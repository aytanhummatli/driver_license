package az.mis.driverlicense.dao.status;


import az.mis.driverlicense.domain.Status;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictStatusDaoImpl implements DictStatusDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Status> listOfStatus() {

        Locale.setDefault(Locale.ENGLISH);

        List<Status> statusList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_status");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("status_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(Status.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            statusList = (List) map.get("status_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return statusList;
    }





}
