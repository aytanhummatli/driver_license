package az.mis.driverlicense.dao.officeType;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import az.mis.driverlicense.domain.OfficeType;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictOfficeTypeDaoImpl implements DictOfficeTypeDao {


    @Autowired
    JdbcTemplate jdbcTemplate;



    public ExceptionModel updateOfficeType(OfficeType officeType, int userId) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("update_office_type");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_note", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("p_id",officeType.getId());
            parameterSource.addValue("p_name",officeType.getName());
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_note", officeType.getNote());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));

            exception.setExceptionMessage((String) map.get("exception_message"));

            exception.setHelperInt((Integer) map.get("p_returning_id"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }



    public ExceptionModel insertOfficeType(OfficeType officeType, int userId) {

        Locale.setDefault(Locale.ENGLISH);

        ExceptionModel exception=new ExceptionModel();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("insert_office_type");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_name", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_note", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_code", OracleTypes.NUMBER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("exception_message", OracleTypes.NVARCHAR));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("p_returning_id", OracleTypes.INTEGER));
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_name",officeType.getName());
            parameterSource.addValue("p_user_id",userId);
            parameterSource.addValue("p_note", officeType.getNote());

            Map map = simpleJdbcCall.execute(parameterSource);

            exception.setExceptionCode((String) map.get("exception_code"));

            exception.setExceptionMessage((String) map.get("exception_message"));

            exception.setHelperInt((Integer) map.get("p_returning_id"));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return exception;
    }

    public List<OfficeType> listOfficeTypes() {

        Locale.setDefault(Locale.ENGLISH);

        List<OfficeType> officeTypes = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_office_type");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("officeType_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(OfficeType.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            officeTypes = (List) map.get("officeType_list");

        } catch (Exception e) {

            e.printStackTrace();
        }

        return officeTypes;
    }



}
