package az.mis.driverlicense.dao.officeType;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import az.mis.driverlicense.domain.OfficeType;

import java.util.List;

public interface DictOfficeTypeDao {

    public List<OfficeType> listOfficeTypes();
    public ExceptionModel insertOfficeType(OfficeType officeType, int userId);
    public ExceptionModel updateOfficeType(OfficeType officeType, int userId);
}
