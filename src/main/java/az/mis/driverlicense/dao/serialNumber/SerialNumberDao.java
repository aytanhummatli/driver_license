package az.mis.driverlicense.dao.serialNumber;

import az.mis.driverlicense.domain.DlSerialNumber;

import java.util.List;


public interface SerialNumberDao {
    public DlSerialNumber getNewSerialNumber(int officeId);
    public List<DlSerialNumber> getSerialNumberbyOffice(int officeId, int userId);
}
