package az.mis.driverlicense.dao.serialNumber;

import az.mis.driverlicense.domain.DlSerialNumber;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class SerialNumberDaoImpl implements SerialNumberDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public  List<DlSerialNumber> getSerialNumberbyOffice(int officeId,int userId) {

        Locale.setDefault(Locale.ENGLISH);

        List<DlSerialNumber> serialNumberArrayList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("get_serial_number_byoffice");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_office_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_user_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("cur", OracleTypes.CURSOR,new BeanPropertyRowMapper(DlSerialNumber.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_office_id",officeId);
            parameterSource.addValue("p_user_id",userId);

            Map map = simpleJdbcCall.execute(parameterSource);

            serialNumberArrayList = (List) map.get("cur");

            System.out.println("serialNumberArrayList  " + serialNumberArrayList.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return serialNumberArrayList;
    }


    public DlSerialNumber getNewSerialNumber(int officeId) {

        Locale.setDefault(Locale.ENGLISH);

        List<DlSerialNumber> serialNumberArrayList = new ArrayList<>();

        DlSerialNumber snObject=new DlSerialNumber();

            try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("LicensePKG").withProcedureName("get_new_serialnumber_byoffice");

            simpleJdbcCall.setAccessCallParameterMetaData(false);
            simpleJdbcCall.addDeclaredParameter(new SqlParameter("p_office_id", OracleTypes.INTEGER));
            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("newSeria", OracleTypes.CURSOR,new BeanPropertyRowMapper(DlSerialNumber.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("p_office_id",officeId);

            Map map = simpleJdbcCall.execute(parameterSource);

            serialNumberArrayList = (List) map.get("newSeria");

            for (DlSerialNumber  elem :serialNumberArrayList){

                snObject=elem;
            }

                System.out.println("snObject  "+snObject.toString());

        } catch (Exception e) {

            e.printStackTrace();
        }

        return snObject;
    }



}
