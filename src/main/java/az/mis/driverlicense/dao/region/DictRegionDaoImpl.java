package az.mis.driverlicense.dao.region;


import az.mis.driverlicense.domain.Region;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Repository
public class DictRegionDaoImpl implements DictRegionDao {


    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Region> listOfRegion() {

        Locale.setDefault(Locale.ENGLISH);

        List<Region> regionList = new ArrayList<>();

        try {

            SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withSchemaName("DRIVERLICENSE")
                    .withCatalogName("dict").withProcedureName("list_of_regions");

            simpleJdbcCall.setAccessCallParameterMetaData(false);

            simpleJdbcCall.addDeclaredParameter(new SqlOutParameter("region_list", OracleTypes.CURSOR, new BeanPropertyRowMapper(Region.class)));

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            Map map = simpleJdbcCall.execute(parameterSource);

            regionList = (List) map.get("region_list");

        } catch (Exception e) {

            e.printStackTrace();

        }

        return regionList;
    }





}
