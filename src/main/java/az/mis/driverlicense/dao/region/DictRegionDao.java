package az.mis.driverlicense.dao.region;


import az.mis.driverlicense.domain.Region;

import java.util.List;

public interface DictRegionDao {

    public List<Region> listOfRegion();
}
