package az.mis.driverlicense.mapper;

import az.mis.driverlicense.domain.DriverLicense;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DriverLicenseMapper implements RowMapper {


    @Override
    public DriverLicense mapRow(ResultSet rs, int i) throws SQLException {

        DriverLicense driverLicense = new DriverLicense();

        driverLicense.setId(rs.getInt("DL_ID"));
        driverLicense.getDlSerialNumber().setDlNumber(rs.getInt("DL_NUMBER"));
        driverLicense.getDlSerialNumber().setSeria(rs.getString("DL_SERIA"));
        driverLicense.getPerson().setId(rs.getInt("person_id"));
        driverLicense.getPerson().setName(rs.getString("NAME"));
        driverLicense.getPerson().setSurName(rs.getString("SURNAME"));
        driverLicense.getPerson().setFatherName(rs.getString("FATHERNAME"));
        driverLicense.getPerson().setBirthday(rs.getDate("BIRTHDAY"));
        driverLicense.setIssueDate(rs.getDate("ISSUE_DATE"));
        driverLicense.setExpireDate(rs.getDate("EXPIRE_DATE"));
        driverLicense.setStatus(rs.getString("status"));
        driverLicense.getDictDlBranch().setBranchText(rs.getString("BranchList"));

        return driverLicense;

    }

}
