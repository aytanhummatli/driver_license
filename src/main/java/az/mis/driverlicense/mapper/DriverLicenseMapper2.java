package az.mis.driverlicense.mapper;

import az.mis.driverlicense.domain.DriverLicense;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DriverLicenseMapper2 implements RowMapper {


    @Override
    public DriverLicense mapRow(ResultSet rs, int i) throws SQLException {

        DriverLicense driverLicense = new DriverLicense();

        driverLicense.setId(rs.getInt("DL_ID"));
        driverLicense.getDlSerialNumber().setDlNumber(rs.getInt("DL_NUMBER"));
        driverLicense.getDlSerialNumber().setSeria(rs.getString("DL_SERIA"));

        driverLicense.getPerson().setId(rs.getInt("person_id"));
        driverLicense.getPerson().setName(rs.getString("person_name"));
        driverLicense.getPerson().setSurName(rs.getString("SURNAME"));
        driverLicense.getPerson().setFatherName(rs.getString("FATHERNAME"));
        driverLicense.getPerson().setBirthday(rs.getDate("BIRTHDAY"));

        driverLicense.getPerson().getBirthPlace().setId(rs.getInt("doguldugu_unvan_id"));
        driverLicense.getPerson().getBirthPlace().setName(rs.getString("doguldugu_unvan_name"));
        driverLicense.getPerson().getBirthPlace().setFullName(rs.getString("doguldugu_unvan_fullname"));

        driverLicense.getPerson().getAddress().setId(rs.getInt("unvan_id"));
        driverLicense.getPerson().getAddress().setName(rs.getString("unvan_name"));
        driverLicense.getPerson().getAddress().setFullName(rs.getString("unvan_fullname"));

        driverLicense.getPerson().getBloodGroup().setId(rs.getInt("blood_id"));
        driverLicense.getPerson().getBloodGroup().setName(rs.getString("blood_name"));

        driverLicense.setBranchText(rs.getString("kateqoriya"));

        driverLicense.setIssueDate(rs.getDate("ISSUE_DATE"));
        driverLicense.setExpireDate(rs.getDate("EXPIRE_DATE"));
        driverLicense.getPerson().getGender().setId(rs.getInt("gender_id"));
        driverLicense.getPerson().getGender().setName(rs.getString("gendername"));
        driverLicense.getPerson().getPersonMobile().setPin(rs.getString("pin"));
        driverLicense.getPerson().getPersonMobile().setMobileNumber(rs.getString("MOBILENUMBER"));
        driverLicense.getReason().setId(rs.getInt("action_id"));
        driverLicense.getSchoolRegistration().getSchool().setId(rs.getInt("ID_SCHOOL"));
        driverLicense.getSchoolRegistration().setSchoolGraduated(rs.getDate("SCHOOL_GRADUATED"));
        driverLicense.getSchoolRegistration().setSchoolRegNumber(rs.getString("SCHOOL_REGNUMBER"));
        driverLicense.getOffice().setId(rs.getInt("OFFICE_ID"));
        driverLicense.getOffice().setName(rs.getString("office_name"));
        driverLicense.getOffice().setNameEn(rs.getString("office_name_en"));

        return driverLicense;

    }



}
