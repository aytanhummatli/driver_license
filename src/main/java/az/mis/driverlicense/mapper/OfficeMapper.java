package az.mis.driverlicense.mapper;

import az.mis.driverlicense.domain.Office;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OfficeMapper implements RowMapper {


    @Override
    public Office mapRow(ResultSet rs, int i) throws SQLException {

        Office office = new Office();


        office.setId(rs.getInt("id"));
        office.setName(rs.getString("name"));
        office.setNameEn(rs.getString("NAME_EN"));
        office.getOfficeType().setId(rs.getInt("OFFICE_TYPE_ID"));
        office.setBackgroundImageDate(rs.getString("backgroundImageDate"));
        office.setStampImageDate(rs.getString("stampImageDate"));
        return office;
    }

}
