package az.mis.driverlicense.mapper;

import az.mis.driverlicense.domain.DictSchool;
import az.mis.driverlicense.domain.Office;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DictSchoolMapper implements RowMapper {


    @Override
    public DictSchool mapRow(ResultSet rs, int i) throws SQLException {

        DictSchool dictSchool = new DictSchool();

        dictSchool.setId(rs.getInt("id"));
        dictSchool.setName(rs.getString("name"));
        dictSchool.setAddress(rs.getString("address"));
        dictSchool.setMobileNumber(rs.getString("mobileNumber"));
        dictSchool.setPhoneNumber(rs.getString("phoneNumber"));
        dictSchool.setVoen(rs.getString("voen"));
        dictSchool.setInsertUserId(rs.getInt("insertUserId"));
        dictSchool.setInsertDate(rs.getDate("insertDate"));
        dictSchool.setPerson(rs.getString("person"));
        dictSchool.getSchoolCompany().setId(rs.getInt("SCHOOL_COMPANY_ID"));

        return dictSchool;
    }

}
