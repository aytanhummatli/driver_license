package az.mis.driverlicense.mapper;

import az.mis.driverlicense.domain.DriverLicense;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ImgSignMapper implements RowMapper {


    @Override
    public DriverLicense mapRow(ResultSet rs, int i) throws SQLException {

        DriverLicense driverLicense = new DriverLicense();
        System.out.println("indttttt  "+rs.getInt("DL_ID"));
        driverLicense.getSignature().setImageData(rs.getString("signature"));
        driverLicense.getPersonImage().setImageData(rs.getString("image"));
        driverLicense.setId(rs.getInt("DL_ID"));

        return driverLicense;

    }

}
