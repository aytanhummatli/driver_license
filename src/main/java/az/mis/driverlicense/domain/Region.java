package az.mis.driverlicense.domain;

public class Region {

    private int id;
    private int idIamasCity;
    private Office office;
    private String name;
    private  String fullName;
    private int isActive;
    private int idParent;
    private int type;
    private int idEqual;

    public Region() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdIamasCity() {
        return idIamasCity;
    }

    public void setIdIamasCity(int idIamasCity) {
        this.idIamasCity = idIamasCity;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIdParent() {
        return idParent;
    }

    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIdEqual() {
        return idEqual;
    }

    public void setIdEqual(int idEqual) {
        this.idEqual = idEqual;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return "Region{" +
                "id=" + id +
                ", idIamasCity=" + idIamasCity +
                ", office=" + office +
                ", name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                ", isActive=" + isActive +
                ", idParent=" + idParent +
                ", type=" + type +
                ", idEqual=" + idEqual +
                '}';
    }
}
