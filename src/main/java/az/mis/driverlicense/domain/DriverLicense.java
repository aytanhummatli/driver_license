package az.mis.driverlicense.domain;

import java.util.Date;

public class DriverLicense  {

    private Date expireDate;
    private Date issueDate;
    private String branchText;
    private String dlSerialNumberText;
    private Office office;
    private int userId;
    private String specialNote;
    private String note;
    private Reason reason;
    private int printCount;
    private int chipCount;
    private Date insertDate;
    private Boolean isForeignDLTest;
    private Boolean canEdit;
    private String dlVersion;
    private Date printDate;
    private Date chipDate;
    private Person person;
    private Integer dlOldVersion;
    private DlSerialNumber dlSerialNumber;
    private int id;
    private int dlOldId;
    private DictDlBranch  dictDlBranch ;
    private SchoolRegistration schoolRegistration;
    private Image personImage;
    private Signature signature;
    private String status;

    public DriverLicense() {

        this.person=new Person();
        this.dlSerialNumber=new DlSerialNumber();
        this.office=new Office();
        this.reason=new Reason();
        this.dictDlBranch=new DictDlBranch();
        this.schoolRegistration=new SchoolRegistration();
        this.personImage=new Image();
        this.signature=new Signature();
    }

    public Image getPersonImage() {
        return personImage;
    }

    public void setPersonImage(Image personImage) {
        this.personImage = personImage;
    }

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    public SchoolRegistration getSchoolRegistration() {
        return schoolRegistration;
    }

    public void setSchoolRegistration(SchoolRegistration schoolRegistration) {
        this.schoolRegistration = schoolRegistration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getBranchText() {
        return branchText;
    }

    public void setBranchText(String branchText) {
        this.branchText = branchText;
    }

    public String getDlSerialNumberText() {
        return dlSerialNumberText;
    }

    public void setDlSerialNumberText(String dlSerialNumberText) {
        this.dlSerialNumberText = dlSerialNumberText;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSpecialNote() {
        return specialNote;
    }

    public void setSpecialNote(String specialNote) {
        this.specialNote = specialNote;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public int getPrintCount() {
        return printCount;
    }

    public void setPrintCount(int printCount) {
        this.printCount = printCount;
    }

    public int getChipCount() {
        return chipCount;
    }

    public void setChipCount(int chipCount) {
        this.chipCount = chipCount;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Boolean getForeignDLTest() {
        return isForeignDLTest;
    }

    public void setForeignDLTest(Boolean foreignDLTest) {
        isForeignDLTest = foreignDLTest;
    }

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    public String getDlVersion() {
        return dlVersion;
    }

    public void setDlVersion(String dlVersion) {
        this.dlVersion = dlVersion;
    }

    public Date getPrintDate() {
        return printDate;
    }

    public void setPrintDate(Date printDate) {
        this.printDate = printDate;
    }

    public Date getChipDate() {
        return chipDate;
    }

    public void setChipDate(Date chipDate) {
        this.chipDate = chipDate;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getDlOldVersion() {
        return dlOldVersion;
    }

    public void setDlOldVersion(Integer dlOldVersion) {
        this.dlOldVersion = dlOldVersion;
    }

    public DlSerialNumber getDlSerialNumber() {
        return dlSerialNumber;
    }

    public void setDlSerialNumber(DlSerialNumber dlSerialNumber) {
        this.dlSerialNumber = dlSerialNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDlOldId() {
        return dlOldId;
    }

    public void setDlOldId(int dlOldId) {
        this.dlOldId = dlOldId;
    }

    public DictDlBranch getDictDlBranch() {
        return dictDlBranch;
    }

    public void setDictDlBranch(DictDlBranch dictDlBranch) {
        this.dictDlBranch = dictDlBranch;
    }

    @Override
    public String toString() {
        return "DriverLicense{" +
                "expireDate=" + expireDate +
                ", issueDate=" + issueDate +
                ", branchText='" + branchText + '\'' +
                ", dlSerialNumberText='" + dlSerialNumberText + '\'' +
                ", office=" + office +
                ", userId=" + userId +
                ", specialNote='" + specialNote + '\'' +
                ", note='" + note + '\'' +
                ", reason=" + reason +
                ", printCount=" + printCount +
                ", chipCount=" + chipCount +
                ", insertDate=" + insertDate +
                ", isForeignDLTest=" + isForeignDLTest +
                ", canEdit=" + canEdit +
                ", dlVersion='" + dlVersion + '\'' +
                ", printDate=" + printDate +
                ", chipDate=" + chipDate +
                ", person=" + person +
                ", dlOldVersion=" + dlOldVersion +
                ", dlSerialNumber=" + dlSerialNumber +
                ", id=" + id +
                ", dlOldId=" + dlOldId +
                ", dictDlBranch=" + dictDlBranch +
                ", schoolRegistration=" + schoolRegistration +
                ", personImage=" + personImage +
                ", signature=" + signature +
                ", status='" + status + '\'' +
                '}';
    }
}
