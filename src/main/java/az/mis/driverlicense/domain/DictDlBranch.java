package az.mis.driverlicense.domain;

public class DictDlBranch {

    private int id;

    private  String name;

    private  String branchText;

    private int isActive;

    public DictDlBranch() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchText() {
        return branchText;
    }

    public void setBranchText(String branchText) {
        this.branchText = branchText;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "DictDlBranch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", branchText='" + branchText + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
