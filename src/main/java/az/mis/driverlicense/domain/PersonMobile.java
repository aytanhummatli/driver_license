package az.mis.driverlicense.domain;

import java.util.Date;

public class PersonMobile {

    private int id;
    private String pin;
    private int isLast;
    private int idInsertUser;
    private Date insertDate;
    private int isActive;
    private String mobileNumber;
    private int personId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getIsLast() {
        return isLast;
    }

    public void setIsLast(int isLast) {
        this.isLast = isLast;
    }

    public int getIdInsertUser() {
        return idInsertUser;
    }

    public void setIdInsertUser(int idInsertUser) {
        this.idInsertUser = idInsertUser;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        return "PersonMobile{" +
                "id=" + id +
                ", pin='" + pin + '\'' +
                ", isLast=" + isLast +
                ", idInsertUser=" + idInsertUser +
                ", insertDate=" + insertDate +
                ", isActive=" + isActive +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", personId=" + personId +
                '}';
    }
}
