package az.mis.driverlicense.domain;

import java.util.Date;

public class SchoolRegistration {

    private int id;
    private School school;
    private String schoolRegNumber;
    private Date schoolGraduated;
    private Date insertDate;
    private int dlId;

    public SchoolRegistration() {
        this.school=new School();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getSchoolRegNumber() {
        return schoolRegNumber;
    }

    public void setSchoolRegNumber(String schoolRegNumber) {
        this.schoolRegNumber = schoolRegNumber;
    }

    public Date getSchoolGraduated() {
        return schoolGraduated;
    }

    public void setSchoolGraduated(Date schoolGraduated) {
        this.schoolGraduated = schoolGraduated;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public int getDlId() {
        return dlId;
    }

    public void setDlId(int dlId) {
        this.dlId = dlId;
    }

    @Override
    public String toString() {
        return "SchoolRegistration{" +
                "id=" + id +
                ", school=" + school +
                ", schoolRegNumber=" + schoolRegNumber +
                ", schoolGraduated=" + schoolGraduated +
                ", insertDate=" + insertDate +
                ", dlId=" + dlId +
                '}';
    }
}
