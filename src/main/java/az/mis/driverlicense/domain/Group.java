package az.mis.driverlicense.domain;

public class Group {

    private int id;

    private  String name;

private int isActive;


    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BloodGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
