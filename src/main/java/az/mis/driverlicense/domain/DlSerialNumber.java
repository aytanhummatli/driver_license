package az.mis.driverlicense.domain;

import java.util.Date;

public class DlSerialNumber {

    private int id;
    private String seria;
    private NsGroup nsGroup;
    private String serialNumberText;
    private  Status dlStatus;
    private int insertUserId;
    private Person usedPersonId;
    private Date insertDate;
    private Date usedDate;
    private Group group;
    private int dlNumber;

    public DlSerialNumber() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeria() {
        return seria;
    }

    public void setSeria(String seria) {
        this.seria = seria;
    }

    public NsGroup getNsGroup() {
        return nsGroup;
    }

    public void setNsGroup(NsGroup nsGroup) {
        this.nsGroup = nsGroup;
    }

    public String getSerialNumberText() {
        return serialNumberText;
    }

    public void setSerialNumberText(String serialNumberText) {
        this.serialNumberText = serialNumberText;
    }

    public Status getDlStatus() {
        return dlStatus;
    }

    public void setDlStatus(Status dlStatus) {
        this.dlStatus = dlStatus;
    }

    public int getInsertUserId() {
        return insertUserId;
    }

    public void setInsertUserId(int insertUserId) {
        this.insertUserId = insertUserId;
    }

    public Person getUsedPersonId() {
        return usedPersonId;
    }

    public void setUsedPersonId(Person usedPersonId) {
        this.usedPersonId = usedPersonId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUsedDate() {
        return usedDate;
    }

    public void setUsedDate(Date usedDate) {
        this.usedDate = usedDate;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public int getDlNumber() {
        return dlNumber;
    }

    public void setDlNumber(int dlNnumber) {
        this.dlNumber = dlNnumber;
    }

    @Override
    public String toString() {
        return "DlSerialNumber{" +
                "id=" + id +
                ", seria='" + seria + '\'' +
                ", nsGroup=" + nsGroup +
                ", serialNumberText='" + serialNumberText + '\'' +
                ", dlStatus=" + dlStatus +
                ", insertUserId=" + insertUserId +
                ", usedPersonId=" + usedPersonId +
                ", insertDate=" + insertDate +
                ", usedDate=" + usedDate +
                ", group=" + group +
                ", dlNumber=" + dlNumber +
                '}';
    }
}
