package az.mis.driverlicense.domain;

public class Image {


    private int id;

    private  int dlId;

    private String imageData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDlId() {
        return dlId;
    }

    public void setDlId(int dlId) {
        this.dlId = dlId;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", dlId=" + dlId +
                ", imageData='" + imageData + '\'' +
                '}';
    }
}
