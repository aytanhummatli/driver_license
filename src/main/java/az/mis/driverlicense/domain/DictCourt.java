package az.mis.driverlicense.domain;

public class DictCourt {

    private int id;
    private String name;
    private int insertUserId;

    public int getInsertUserId() {
        return insertUserId;
    }

    public void setInsertUserId(int insertUserId) {
        this.insertUserId = insertUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DictGender{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
