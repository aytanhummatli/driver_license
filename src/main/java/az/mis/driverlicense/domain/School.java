package az.mis.driverlicense.domain;

import java.util.Date;

public class School {


    private int id;
    private String name;
    private String address;
    private String mobileNumber;
    private String phoneNumber;
    private String voen;
    private SchoolCompany schoolCompany;
    private int insertUserId;
    private Date insertDate;
    private String person;

    public School() {
        this.schoolCompany=new SchoolCompany();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getVoen() {
        return voen;
    }

    public void setVoen(String voen) {
        this.voen = voen;
    }

    public SchoolCompany getSchoolCompany() {
        return schoolCompany;
    }

    public void setSchoolCompany(SchoolCompany schoolCompany) {
        this.schoolCompany = schoolCompany;
    }

    public int getInsertUserId() {
        return insertUserId;
    }

    public void setInsertUserId(int insertUserId) {
        this.insertUserId = insertUserId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", voen='" + voen + '\'' +
                ", schoolCompany=" + schoolCompany +
                ", insertUserId=" + insertUserId +
                ", insertDate=" + insertDate +
                ", person='" + person + '\'' +
                '}';
    }
}
