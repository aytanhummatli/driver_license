package az.mis.driverlicense.domain;

import java.util.Date;

public class SchoolCompany {

    private int id;
    private  String name;
    private int insertUserId;
    private Date insertDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInsertUserId() {
        return insertUserId;
    }

    public void setInsertUserId(int insertUserId) {
        this.insertUserId = insertUserId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public String toString() {
        return "SchoolCompany{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", insertUserId=" + insertUserId +
                ", insertDate=" + insertDate +
                '}';
    }
}
