package az.mis.driverlicense.domain;

import java.util.List;

public class Users {


    private int id;

    private String password;

    private String name;

    private List<Rule> rules;


    public Users() {
    }

    public Users(Users user) {

        this.id = user.id;

        this.password = user.password;
        this.name = user.name;

        this.rules = user.rules;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", rules=" + rules +
                '}';
    }
}
