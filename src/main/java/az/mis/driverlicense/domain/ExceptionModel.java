package az.mis.driverlicense.domain;

public class ExceptionModel {

    private String exceptionCode;
    private String exceptionMessage;
    private int helperInt;
    private String helperString;

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public int getHelperInt() {
        return helperInt;
    }

    public void setHelperInt(int helperInt) {
        this.helperInt = helperInt;
    }

    public String getHelperString() {
        return helperString;
    }

    public void setHelperString(String helperString) {
        this.helperString = helperString;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }


    @Override
    public String toString() {
        return "ExceptionModel{" +
                "exceptionCode='" + exceptionCode + '\'' +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                '}';
    }


}
