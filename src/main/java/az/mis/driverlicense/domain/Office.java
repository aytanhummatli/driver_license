package az.mis.driverlicense.domain;

import java.util.Arrays;

public class Office {

    private int id;
    private String name;
    private String stampImageDate;
    private String  backgroundImageDate;
    private String nameEn;
    private  int orderBy;
    private OfficeType officeType;

    public Office() {
        this.officeType=new OfficeType();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStampImageDate() {
        return stampImageDate;
    }

    public void setStampImageDate(String stampImageDate) {
        this.stampImageDate = stampImageDate;
    }

    public String getBackgroundImageDate() {
        return backgroundImageDate;
    }

    public void setBackgroundImageDate(String backgroundImageDate) {
        this.backgroundImageDate = backgroundImageDate;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public OfficeType getOfficeType() {
        return officeType;
    }

    public void setOfficeType(OfficeType officeType) {
        this.officeType = officeType;
    }

    @Override
    public String toString() {
        return "Office{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stampImageDate='" + stampImageDate + '\'' +
                ", backgroundImageDate='" + backgroundImageDate + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", orderBy=" + orderBy +
                ", officeType=" + officeType +
                '}';
    }
}
