package az.mis.driverlicense.domain;

import java.util.Date;

public class NsGroup {

    private int id;
    private Group group;
    private String seria;
    private int fromNumber;
    private int toNumber;
    private int insertUserId;
    private Date insertDate;
    private int isActive;
    private Office office;


    public NsGroup() {
    }

    public int getInsertUserId() {
        return insertUserId;
    }

    public void setInsertUserId(int insertUserId) {
        this.insertUserId = insertUserId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public int getToNumber() {
        return toNumber;
    }

    public void setToNumber(int toNumber) {
        this.toNumber = toNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getSeria() {
        return seria;
    }

    public void setSeria(String seria) {
        this.seria = seria;
    }

    public int getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(int fromNumber) {
        this.fromNumber = fromNumber;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return "NsGroup{" +
                "id=" + id +
                ", group=" + group +
                ", seria='" + seria + '\'' +
                ", fromNumber=" + fromNumber +
                ", toNumber=" + toNumber +
                ", insertUserId=" + insertUserId +
                ", insertDate=" + insertDate +
                ", isActive=" + isActive +
                ", office=" + office +
                '}';
    }
}
