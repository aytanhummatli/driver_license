package az.mis.driverlicense.domain;


import az.mis.driverlicense.controller.CommonClass;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Person {


    private int id;
    private String pin;
    private String idCardSeria;
    private String idCardNumber;
    private  String name;
    private String surName;
    private String fatherName;
    private DictGender gender;

    private Date birthday;
    //    private ResidenceCity residenceCityId;
    //    private ResidenceDistrict residenceDistrictId;
    private Region residenceCityId;
    private Region residenceDistrictId;
    private BloodGroup bloodGroup;
    private Region address;
    private String citizenship;
    private String iamasStatus;
    private PersonType personType;
    private String residencePlaceDetail;
    private String mobileNumber;
    private int isFromIamas;
    private Date insertDate;
    private String birthDateString;
    private  String namEn;
    private String surNamEn;
    private String fatherNamEn;
    private String birthPlacEn;
    private Region birthPlace;
    private PersonMobile personMobile;
    private String status;



    public Person() {

        this.address=new Region();
        this.bloodGroup=new BloodGroup();
        this.personType=new PersonType();
        this.birthPlace=new Region();
        this.gender=new DictGender();
        this.personMobile=new PersonMobile();

    }


    public String getBirthDateString() {
        return birthDateString;
    }

    public void setBirthDateString(String birthDateString) {
        this.birthDateString = birthDateString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getIdCardSeria() {
        return idCardSeria;
    }

    public void setIdCardSeria(String idCardSeria) {
        this.idCardSeria = idCardSeria;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public DictGender getGender() {
        return gender;
    }

    public void setGender(DictGender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {

        this.birthday = CommonClass.toSqlDate(birthday);
        System.out.println("  this.birthday "+this.birthday );
    }

    public Region getResidenceCityId() {
        return residenceCityId;
    }

    public void setResidenceCityId(Region residenceCityId) {
        this.residenceCityId = residenceCityId;
    }

    public Region getResidenceDistrictId() {
        return residenceDistrictId;
    }

    public void setResidenceDistrictId(Region residenceDistrictId) {
        this.residenceDistrictId = residenceDistrictId;
    }

    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(BloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getIamasStatus() {
        return iamasStatus;
    }

    public void setIamasStatus(String iamasStatus) {
        this.iamasStatus = iamasStatus;
    }

    public PersonType getPersonType() {
        return personType;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }

    public String getResidencePlaceDetail() {
        return residencePlaceDetail;
    }

    public void setResidencePlaceDetail(String residencePlaceDetail) {
        this.residencePlaceDetail = residencePlaceDetail;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getIsFromIamas() {
        return isFromIamas;
    }

    public void setIsFromIamas(int isFromIamas) {
        this.isFromIamas = isFromIamas;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getNamEn() {
        return namEn;
    }

    public void setNamEn(String namEn) {
        this.namEn = namEn;
    }

    public String getSurNamEn() {
        return surNamEn;
    }

    public void setSurNamEn(String surNamEn) {
        this.surNamEn = surNamEn;
    }

    public String getFatherNamEn() {
        return fatherNamEn;
    }

    public void setFatherNamEn(String fatherNamEn) {
        this.fatherNamEn = fatherNamEn;
    }

    public String getBirthPlacEn() {
        return birthPlacEn;
    }

    public void setBirthPlacEn(String birthPlacEn) {
        this.birthPlacEn = birthPlacEn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Region getAddress() {
        return address;
    }

    public void setAddress(Region address) {
        this.address = address;
    }

    public Region getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(Region birthPlace) {
        this.birthPlace = birthPlace;
    }

    public PersonMobile getPersonMobile() {
        return personMobile;
    }

    public void setPersonMobile(PersonMobile personMobile) {
        this.personMobile = personMobile;
    }


    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", pin='" + pin + '\'' +
                ", idCardSeria='" + idCardSeria + '\'' +
                ", idCardNumber='" + idCardNumber + '\'' +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", gender=" + gender +
                ", birthday=" + birthday +
                ", residenceCityId=" + residenceCityId +
                ", residenceDistrictId=" + residenceDistrictId +
                ", bloodGroup=" + bloodGroup +
                ", address=" + address +
                ", citizenship='" + citizenship + '\'' +
                ", iamasStatus='" + iamasStatus + '\'' +
                ", personType=" + personType +
                ", residencePlaceDetail='" + residencePlaceDetail + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", isFromIamas=" + isFromIamas +
                ", insertDate=" + insertDate +
                ", birthDateString='" + birthDateString + '\'' +
                ", namEn='" + namEn + '\'' +
                ", surNamEn='" + surNamEn + '\'' +
                ", fatherNamEn='" + fatherNamEn + '\'' +
                ", birthPlacEn='" + birthPlacEn + '\'' +
                ", birthPlace=" + birthPlace +
                ", personMobile=" + personMobile +
                ", status='" + status + '\'' +

                '}';
    }


}
