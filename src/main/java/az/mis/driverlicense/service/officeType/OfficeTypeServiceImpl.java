package az.mis.driverlicense.service.officeType;

import az.mis.driverlicense.dao.officeType.DictOfficeTypeDao;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.OfficeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OfficeTypeServiceImpl implements  OfficeTypeService {

    @Autowired
    DictOfficeTypeDao officeTypeDao;

    public List<OfficeType> listOfficeTypes(){
        return officeTypeDao.listOfficeTypes();
    }


    public ExceptionModel insertOfficeType(OfficeType officeType, int userId) {
        return officeTypeDao.insertOfficeType(officeType,userId);
    }


    public ExceptionModel updateOfficeType(OfficeType officeType, int userId) {
        return officeTypeDao.updateOfficeType(officeType,userId);
    }


}
