package az.mis.driverlicense.service.officeType;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.OfficeType;

import java.util.List;

public interface OfficeTypeService {
    public List<OfficeType> listOfficeTypes();
    public ExceptionModel insertOfficeType(OfficeType officeType, int userId);
    public ExceptionModel updateOfficeType(OfficeType officeType, int userId);
}
