package az.mis.driverlicense.service.restrictedDocStatus;

import az.mis.driverlicense.dao.restrictedDocStatus.RestrictedDocStatusDao;
import az.mis.driverlicense.domain.RestrictedDocStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestrictedDocStatusServiceImpl implements RestrictedDocStatusService {



   @Autowired
    RestrictedDocStatusDao restrictedDocStatusDao;

    public List<RestrictedDocStatus> listOfRestrictedDocStatus (){

        return restrictedDocStatusDao.listOfRestrictedDocStatus();
    }


}
