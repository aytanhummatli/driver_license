package az.mis.driverlicense.service.restrictedDocStatus;

import az.mis.driverlicense.domain.RestrictedDocStatus;

import java.util.List;

public interface RestrictedDocStatusService {
    public List<RestrictedDocStatus> listOfRestrictedDocStatus ();
}
