package az.mis.driverlicense.service.dictDlBranch;

import az.mis.driverlicense.domain.DictDlBranch;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public interface DictDlBranchService {

    public List<DictDlBranch> listOfDlBranch();
}
