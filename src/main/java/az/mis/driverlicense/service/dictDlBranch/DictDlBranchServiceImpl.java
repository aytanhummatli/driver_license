package az.mis.driverlicense.service.dictDlBranch;

import az.mis.driverlicense.dao.dictDlBranch.DictDlBranchDao;
import az.mis.driverlicense.domain.DictDlBranch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DictDlBranchServiceImpl implements DictDlBranchService {

    @Autowired
    DictDlBranchDao dictDlBranchDao;


    public List<DictDlBranch> listOfDlBranch(){

        return dictDlBranchDao.listOfDlBranch();
    }
}
