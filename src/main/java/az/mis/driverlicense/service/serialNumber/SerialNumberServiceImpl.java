package az.mis.driverlicense.service.serialNumber;

import az.mis.driverlicense.dao.serialNumber.SerialNumberDao;
import az.mis.driverlicense.domain.DlSerialNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SerialNumberServiceImpl implements  SerialNumberService {



    @Autowired
    SerialNumberDao serialNumberDao;


    public DlSerialNumber getNewSerialNumber(int officeId) {

        return serialNumberDao.getNewSerialNumber(officeId);
    }

    public List<DlSerialNumber> getSerialNumberbyOffice(int officeId, int userId){

        return serialNumberDao.getSerialNumberbyOffice(officeId,userId);
    }

}
