package az.mis.driverlicense.service.serialNumber;

import az.mis.driverlicense.domain.DlSerialNumber;

import java.util.List;

public interface SerialNumberService {
    public DlSerialNumber getNewSerialNumber(int officeId);
    public List<DlSerialNumber> getSerialNumberbyOffice(int officeId, int userId);
}
