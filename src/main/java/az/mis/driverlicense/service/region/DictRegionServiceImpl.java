package az.mis.driverlicense.service.region;


import az.mis.driverlicense.dao.region.DictRegionDao;

import az.mis.driverlicense.domain.Region;
import az.mis.driverlicense.service.bloodGroup.DictBloodGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictRegionServiceImpl implements  DictRegionService {

    @Autowired
    DictRegionDao regionDao;


    public List<Region> listOfRegion(){

        return regionDao.listOfRegion();
    }
}
