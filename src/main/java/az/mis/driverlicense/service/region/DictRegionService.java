package az.mis.driverlicense.service.region;

import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.Region;

import java.util.List;

public interface DictRegionService {
    public List<Region> listOfRegion();
}
