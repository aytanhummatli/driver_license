package az.mis.driverlicense.service.driverLicense;

import az.mis.driverlicense.dao.driverLicense.DriverLicenseDao;
import az.mis.driverlicense.domain.DriverLicense;

import az.mis.driverlicense.domain.ExceptionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DriverLicenseServiceImpl implements DriverLicenseService {

    @Autowired
    DriverLicenseDao driverLicenseDao;


    public List<DriverLicense> listDlMaingridByUser(int officeId, String dlSstatus) {

        return driverLicenseDao.listDlMaingridByUser(officeId, dlSstatus);
    }

    public DriverLicense getAllFieldsByDLID(int dlId) {

        return driverLicenseDao.getAllFieldsByDLID(dlId);
    }

    public List<DriverLicense> listDlMaingridSearch(DriverLicense driverLicense, String searchLike){

        return  driverLicenseDao.listDlMaingridSearch(driverLicense,searchLike);
    }


    public ExceptionModel updateDlStatus(DriverLicense driverLicense){

        return  driverLicenseDao.updateDlStatus(driverLicense);
    }



}