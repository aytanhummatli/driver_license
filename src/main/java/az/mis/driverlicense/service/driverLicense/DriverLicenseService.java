package az.mis.driverlicense.service.driverLicense;

import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;

import java.util.List;

public interface DriverLicenseService {

    public List<DriverLicense> listDlMaingridByUser(int officeId, String dlSstatus);
    public DriverLicense getAllFieldsByDLID(int dlId);
    public List<DriverLicense> listDlMaingridSearch(DriverLicense driverLicense, String searchLike);

    public ExceptionModel updateDlStatus(DriverLicense driverLicense);
}
