package az.mis.driverlicense.service.personMobile;

import az.mis.driverlicense.dao.personMobile.PersonMobileDao;

import az.mis.driverlicense.domain.PersonMobile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonMobileServiceImpl implements  PersonMobileService {
    @Autowired
    PersonMobileDao personMobileDao;


    public List<PersonMobile> listPersonMobile(int personId) {

        return personMobileDao.listPersonMobile(personId);
    }
}
