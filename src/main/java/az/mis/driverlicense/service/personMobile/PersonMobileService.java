package az.mis.driverlicense.service.personMobile;

import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.PersonMobile;

import java.util.List;

public interface PersonMobileService {
    public List<PersonMobile> listPersonMobile(int personId);
}
