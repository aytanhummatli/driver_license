package az.mis.driverlicense.service.dictOffice;


import az.mis.driverlicense.dao.dictOffice.DictOfficeDao;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictOfficeServiceImpl implements DictOfficeService {

    @Autowired
    DictOfficeDao dictOfficeDao;

    public List<Office> listOfOffices() {

        return dictOfficeDao.listOfOffices();
    }

    public  Office  listOfFullOfficesById(int userId,int officeId){

        return dictOfficeDao.listOfFullOfficesById(userId,officeId);
    }
    public   List<Office> searchOfficeByName( int userId, String name) {

        return dictOfficeDao.searchOfficeByName(userId,name);
    }


    public ExceptionModel updateOffice(Office office, int userId){

        return dictOfficeDao.updateOffice(office,userId);
    }

    public ExceptionModel insertOffice(Office office, int userId) {

        return dictOfficeDao.insertOffice(office,userId);
    }

    public ExceptionModel deleteOffice(int officeId, int userId) {

        return dictOfficeDao.deleteOffice(officeId,userId);
    }



}
