package az.mis.driverlicense.service.dictOffice;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Office;

import java.util.List;

public interface DictOfficeService {

    public List<Office> listOfOffices();
    public ExceptionModel updateOffice(Office office, int userId);
    public ExceptionModel insertOffice(Office office, int userId);
    public ExceptionModel deleteOffice(int officeId, int userId);

    public  Office  listOfFullOfficesById(int userId,int officeId);
    public  List<Office> searchOfficeByName( int userId, String name);

}
