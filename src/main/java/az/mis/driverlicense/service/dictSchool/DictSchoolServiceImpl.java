package az.mis.driverlicense.service.dictSchool;

import az.mis.driverlicense.dao.dictSchoolDao.DictSchoolDao;
import az.mis.driverlicense.domain.DictSchool;
import az.mis.driverlicense.domain.ExceptionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictSchoolServiceImpl implements DictSchoolService {

    @Autowired
    DictSchoolDao dictSchoolDao;

    public List<DictSchool> listOfSchools() {

        return dictSchoolDao.listOfSchools();
    }

    public  List<DictSchool>  listOfSchoolsbyName( int userId, String name) {

        return dictSchoolDao.listOfSchoolsbyName(userId, name);
    }

    public ExceptionModel updateSchool(DictSchool dictSchool){
        return dictSchoolDao.updateSchool(dictSchool);

    }
    public ExceptionModel insertSchool(DictSchool dictSchool){
        return dictSchoolDao.insertSchool(dictSchool);

    }

    public ExceptionModel deleteSchool(DictSchool dictSchool){
        return dictSchoolDao.deleteSchool(dictSchool);
    }

}
