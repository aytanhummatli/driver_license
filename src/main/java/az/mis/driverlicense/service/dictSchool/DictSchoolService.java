package az.mis.driverlicense.service.dictSchool;

import az.mis.driverlicense.domain.DictSchool;
import az.mis.driverlicense.domain.ExceptionModel;

import java.util.List;

public interface DictSchoolService {

    public List<DictSchool> listOfSchools();
    public  List<DictSchool>  listOfSchoolsbyName( int userId, String name);
    public ExceptionModel updateSchool(DictSchool dictSchool);
    public ExceptionModel insertSchool(DictSchool dictSchool);
    public ExceptionModel deleteSchool(DictSchool dictSchool);


}
