package az.mis.driverlicense.service.imageSignature;


import az.mis.driverlicense.dao.imageAndSignature.ImageSignatureDao;
import az.mis.driverlicense.domain.DriverLicense;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.Image;
import az.mis.driverlicense.domain.Signature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageSignatureServiceImpl implements  ImageSignatureService {


    @Autowired
    ImageSignatureDao imageSignatureDao;


    public ExceptionModel insertSignature(int userId, Signature signature)  {

        return imageSignatureDao.insertSignature(userId,signature);
    }
    public ExceptionModel insertImage(int userId, Image image)  {

        return imageSignatureDao.insertImage(userId,image);
    }

    public DriverLicense getImageSignatureByDlId(int dlId){

        return imageSignatureDao.getImageSignatureByDlId(dlId);
    }

    public String getStampImageByOfficeId(int officeId){

        return imageSignatureDao.getStampImageByOfficeId(officeId);
    }
    public String getBackgroundImageByOfficeId(int officeId){


        return imageSignatureDao.getBackgroundImageByOfficeId(officeId);
    }

}
