package az.mis.driverlicense.service.imageSignature;

import az.mis.driverlicense.domain.*;

import java.util.List;

public interface ImageSignatureService  {
    public ExceptionModel insertSignature(int userId, Signature signature) ;
    public ExceptionModel insertImage(int userId, Image image);
    public DriverLicense getImageSignatureByDlId(int dlId);
    public String getStampImageByOfficeId(int officeId);
    public String getBackgroundImageByOfficeId(int officeId);

}
