package az.mis.driverlicense.service.bloodGroup;

import az.mis.driverlicense.domain.BloodGroup;
import az.mis.driverlicense.domain.DictCourt;

import java.util.List;

public interface DictBloodGroupService {
    public List<BloodGroup> listOfBloodGroup();
}
