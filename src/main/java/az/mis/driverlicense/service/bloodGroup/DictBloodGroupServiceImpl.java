package az.mis.driverlicense.service.bloodGroup;

import az.mis.driverlicense.dao.bloodGroup.DictBloodGroupDao;

import az.mis.driverlicense.domain.BloodGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictBloodGroupServiceImpl implements  DictBloodGroupService {
    @Autowired
    DictBloodGroupDao dictBloodGroupDao;


    public List<BloodGroup> listOfBloodGroup() {

        return dictBloodGroupDao.listOfBloodGroup();
    }
}
