package az.mis.driverlicense.service.dictGender;

import az.mis.driverlicense.dao.dictGender.DictGenderDao;
import az.mis.driverlicense.domain.DictGender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DictGenderServiceImpl implements  DictGenderService {
    @Autowired
    DictGenderDao dictGenderDao;


    public List<DictGender> listOfGender(){

        return dictGenderDao.listOfGender();
    }
}
