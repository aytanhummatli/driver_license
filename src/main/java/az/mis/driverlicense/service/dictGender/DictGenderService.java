package az.mis.driverlicense.service.dictGender;

import az.mis.driverlicense.domain.DictGender;

import java.util.List;

public interface DictGenderService {
    public List<DictGender> listOfGender();
}
