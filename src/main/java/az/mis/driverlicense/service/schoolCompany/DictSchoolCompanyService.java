package az.mis.driverlicense.service.schoolCompany;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.SchoolCompany;

import java.util.List;

public interface DictSchoolCompanyService {
    public List<SchoolCompany> listOfSchoolCompany();
    public ExceptionModel deleteSchoolCompany(SchoolCompany schoolCompany);
    public ExceptionModel updateSchoolCompany(SchoolCompany schoolCompany);
    public ExceptionModel insertSchoolCompany(SchoolCompany schoolCompany);
}
