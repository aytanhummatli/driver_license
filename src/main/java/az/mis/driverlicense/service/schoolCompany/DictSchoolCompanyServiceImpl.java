package az.mis.driverlicense.service.schoolCompany;

import az.mis.driverlicense.dao.schoolCompany.DictSchoolCompanyDao;

import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.SchoolCompany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictSchoolCompanyServiceImpl implements  DictSchoolCompanyService {
    @Autowired
    DictSchoolCompanyDao dictSchoolCompanyDao;


    public List<SchoolCompany> listOfSchoolCompany() {

        return dictSchoolCompanyDao.listOfSchoolCompany();
    }


    @Override
    public ExceptionModel deleteSchoolCompany(SchoolCompany schoolCompany) {
        return dictSchoolCompanyDao.deleteSchoolCompany(schoolCompany);
    }

    @Override
    public ExceptionModel updateSchoolCompany(SchoolCompany schoolCompany) {
        return dictSchoolCompanyDao.updateSchoolCompany(schoolCompany);
    }

    @Override
    public ExceptionModel insertSchoolCompany(SchoolCompany schoolCompany) {
        return dictSchoolCompanyDao.insertSchoolCompany(schoolCompany);
    }
}
