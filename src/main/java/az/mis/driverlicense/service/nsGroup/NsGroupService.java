package az.mis.driverlicense.service.nsGroup;


import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.NsGroup;
import az.mis.driverlicense.domain.Office;

import java.util.List;

public interface NsGroupService {
    public ExceptionModel insertNsGroup(NsGroup nsGroup);
    public  List<NsGroup> getNsGroupsbyOffice(int  userId ,int officeId);


}
