package az.mis.driverlicense.service.nsGroup;


import az.mis.driverlicense.dao.nsGroup.NsGroupDao;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.domain.NsGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NsGroupServiceImpl implements NsGroupService {

    @Autowired
    NsGroupDao nsGroupDao;


    public ExceptionModel insertNsGroup(NsGroup nsGroup){

       return  nsGroupDao.insertNsGroup(nsGroup) ;
    }


    public List<NsGroup> getNsGroupsbyOffice(int  userId , int officeId){

        return  nsGroupDao.getNsGroupsbyOffice(userId,officeId) ;
    }


}
