package az.mis.driverlicense.service.dictCourt;

import az.mis.driverlicense.dao.dictCourt.DictCourtDao;
import az.mis.driverlicense.dao.dictGender.DictGenderDao;
import az.mis.driverlicense.domain.DictCourt;
import az.mis.driverlicense.domain.DictGender;
import az.mis.driverlicense.domain.ExceptionModel;
import az.mis.driverlicense.service.dictGender.DictGenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictCourtServiceImpl implements  DictCourtService {
    @Autowired
    DictCourtDao dictCourtDao;


    public List<DictCourt> listOfCourts() {

        return dictCourtDao.listOfCourts();
    }

    @Override
    public ExceptionModel deleteCourt(DictCourt dictCourt) {
        return dictCourtDao.deleteCourt(dictCourt);
    }

    @Override
    public ExceptionModel updateCourt(DictCourt dictCourt) {
        return dictCourtDao.updateCourt(dictCourt);
    }

    @Override
    public ExceptionModel insertCourt(DictCourt dictCourt) {
        return dictCourtDao.insertCourt(dictCourt);
    }
}
