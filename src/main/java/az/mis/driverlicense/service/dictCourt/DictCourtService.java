package az.mis.driverlicense.service.dictCourt;

import az.mis.driverlicense.domain.DictCourt;
import az.mis.driverlicense.domain.DictGender;
import az.mis.driverlicense.domain.ExceptionModel;

import java.util.List;

public interface DictCourtService {
    public List<DictCourt> listOfCourts() ;
    public ExceptionModel deleteCourt(DictCourt dictCourt);
    public ExceptionModel updateCourt(DictCourt dictCourt);
    public ExceptionModel insertCourt(DictCourt dictCourt);
}
