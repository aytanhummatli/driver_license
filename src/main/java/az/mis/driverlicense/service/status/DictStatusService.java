package az.mis.driverlicense.service.status;


import az.mis.driverlicense.domain.Status;

import java.util.List;

public interface DictStatusService {
    public List<Status> listOfStatus();
}
