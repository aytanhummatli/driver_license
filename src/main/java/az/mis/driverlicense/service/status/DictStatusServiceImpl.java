package az.mis.driverlicense.service.status;


import az.mis.driverlicense.dao.status.DictStatusDao;
import az.mis.driverlicense.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class DictStatusServiceImpl implements  DictStatusService {

    @Autowired
    DictStatusDao statusDao;


    public List<Status> listOfStatus() {

        return statusDao.listOfStatus();
    }
}
