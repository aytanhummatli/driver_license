package az.mis.driverlicense.service.dictReason;

import az.mis.driverlicense.dao.dictCourt.DictCourtDao;
import az.mis.driverlicense.dao.reason.DictReasonDao;
import az.mis.driverlicense.domain.DictCourt;
import az.mis.driverlicense.domain.Reason;
import az.mis.driverlicense.service.dictCourt.DictCourtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictReasonServiceImpl implements  DictReasonService {

    @Autowired
    DictReasonDao dictReasonDao;

    public List<Reason> listOfReasons() {
        return dictReasonDao.listOfReasons();
    }


}
