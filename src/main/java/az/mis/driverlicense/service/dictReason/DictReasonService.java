package az.mis.driverlicense.service.dictReason;

import az.mis.driverlicense.domain.DictCourt;
import az.mis.driverlicense.domain.Reason;

import java.util.List;




public interface DictReasonService {

    public List<Reason> listOfReasons() ; }
